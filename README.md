# Elite Dangerous - HOTAS Binds

For players of ED a repository of bindings. Feel free to put in a MR and contribute!

Couple of relevant links for Binds management for ED:

- [https://edrefcard.info/](EDRefCard) - great site for all kinds of binds. No search so it's painful to navigate
- [https://edrefcard.info/list](EDRefCard - all bindings)
- [https://edrefcard.info/device/TFlightHOTASX](Thrustmaster Flight HOTAS X reference image)

# Folder structure (WIP)

- Stick Name
  - Binds in .bind format
  - Possible images
  - Links to EDRefCard
